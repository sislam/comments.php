<?php
    /*
     * If the current post is protected by a password and
     * the visitor has not yet entered the password we will
     * return early without loading the comments.
     */
    if ( post_password_required() )
        return;
?>
 
<?php if ( have_comments() ) : ?>
	<h4><?php comments_number( '0 Comment', '1 Comment', '% Comments'); ?></h4>
    <?php wp_list_comments( array( 'callback' => 'xtrimIT_comment' ) );?>


    <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
    <nav role="navigation" id="comment-nav-below" class="site-navigation comment-navigation">
        <h1 class="assistive-text"><?php __( 'Comment navigation', 'persona' ); ?></h1>
        <div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'persona' ) ); ?></div>
        <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'persona' ) ); ?></div>
    </nav>
    <?php endif;?>

<?php endif; ?>

<?php
    // If comments are closed and there are comments, let's leave a little note, shall we?
    if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
?>
    <p class="nocomments"><?php __( 'Comments are closed.', 'persona' ); ?></p>
<?php endif; ?>

<?php 

    $com_args = array(
            // <h4>Add your comment Here</h4>
            'comment_field' =>  '<textarea placeholder="your massage*" id="description" class="form-control" id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>',
            'id_form'           => 'contact-form',
            'label_submit' => __('Send message', 'persona'),
            'id_submit'           => 'submitButton',
            'class_submit'           => 'button btn-contact-bg',
            'title_reply'       => '<h4>Add your comment Here</h4>',
            // 'class_form'      => 'comment-form',
        );



    comment_form($com_args); 

?>