

if ( ! function_exists( 'xtrimIT_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Shape 1.0
 */
	function xtrimIT_comment( $comment, $args, $depth ) {
		$ava_arg = array(
			'class' => 'img-circle',
			);
	    $GLOBALS['comment'] = $comment;
	    switch ( $comment->comment_type ) :
	        case 'pingback' :
	        case 'trackback' :
	    ?>
	    <li class="post pingback">
	        <p><?php __( 'Pingback:', 'persona' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', 'persona' ), ' ' ); ?></p>
	    <?php
	            break;
	        default :
	    ?>
	    <div class="post-comment <?php echo implode(" ",get_comment_class()); ?>" id="li-comment-<?php comment_ID(); ?>">

			<blockquote class="single-comment">
			    <?php echo get_avatar( $comment, 100, '', '', $ava_arg); ?>
			    <h5><?php echo ucfirst(get_comment_author()); ?></h5>
			    <span><i class="fa fa-calendar"></i> <?php echo get_comment_date(); ?></span>
			    <p><?php comment_text(); ?></p>
			    <span class="reply-button"><i class="fa fa-reply"></i> <?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?></span> 
			</blockquote>
			<div class="clearfix"></div>
		</div>
	    <?php
	            break;
	    endswitch;
	}
endif; 


